def buildJar() {
    echo 'building the application'
    sh 'npm install'
    sh 'docker build . -t dolev85/nodejs_app:2.0'
}

def testApp() {
    echo 'testing the application'
}

def deployApp() {
    echo 'deploying the application'
}